; Skylar2600
; Gift Project for Skylar Baker's 23rd birthday
; Code by Andrea Pascal

                processor 6502 
                include "vcs.h" 
                include "macro.h" 

;/////////////////  Start of Code ///////////////////////////////////// 

                include "locals.h"

                SEG 
                ORG $F000 

Reset 
            ; Clear RAM, TIA registers and Set Stack Pointer to #$FF
        SEI
        CLD	
        LDX #$FF
        TXS
        LDA #0
Clear_Mem
        STA 0,X
        DEX
        BNE Clear_Mem			

        LDA #$00
        STA COLUBK	; Set Background to Black
        LDA #$52        ; Set default Playfield colour (Skylar's favourite one)
        STA COLUPF
        LDA #%00000001  ; Mirrored Playfield
        STA CTRLPF

        ; Add default colours to the WRAM
        LDA #$64
        STA _Palette_A
        LDA #$74
        STA _Palette_B
        LDA #$84
        STA _Palette_C
        LDA #$94
        STA _Palette_D

        ; Add frame counter and palette counter to the WRAM
        LDA #0
        STA _Frame_Counter
        STA _Palette_Pointer

        ; Wipe TIA audio registers
        LDA #0
        STA AUDV0
        STA AUDV1
        STA AUDC0
        STA AUDC1
        STA AUDF0
        STA AUDF1

        ; Add audio note counter and pointers to the WRAM

        LDA #1
        STA _Note_Counter

        LDA #<Music
        STA _Music_Pointer+0
        LDA #>Music
        STA _Music_Pointer+1

;///////////////////  Picture Starts Here /////////////////////////////

Start_Frame 

        ; Start VSYNC

                LDA #2 
                STA VSYNC 

                STA WSYNC 
                STA WSYNC 
                STA WSYNC    	; 3 Scanlines of VSYNC 

                LDA #0 
                STA VSYNC	; End VSYNC         

        ; VSync_Palette Pick

                LDA _Frame_Counter
                CMP #8   ; Jump every 8 frames
                BEQ VSync_Palette
                STA WSYNC ; Add an empty scanline on the other 7 frames

Start_Frame2

        INC _Frame_Counter

;//////////////// Music Playback Starts Here /////////////////////

        DEC _Note_Counter
        BEQ FetchNote
        STA WSYNC

;//////////////// Spin lock Vertical Blank ////////////////////////

Start_Frame3

               ; 32 Scanlines of Vertical Blank... 
               LDX #32

Vertical_Blank

                STA WSYNC 
                DEX 
                BNE Vertical_Blank 
                
                LDA #0 
                STA VBLANK 	; Enable TIA Output
                                
;////////////// Start To Draw Playfield ///////////////////////////////      	
          
        LDX #192        ; 192 Scanlines to Display
        LDY #$00        ; Index for current frame's palette

Draw_Picture            ; Draw the scanline
        LDA Screen_PF0-1,X
        STA PF0
        LDA Screen_PF1-1,X
        STA PF1
        LDA Screen_PF2-1,X
        STA PF2
        STA WSYNC

        TXA             ; Check if the colour needs to change for the next line
        AND #%00001111  ; Change Playfield colour every 16 scalines
        BEQ HSync_Palette

Draw_Picture2
        DEX             ; Check if we're done drawing this frame
        BNE Draw_Picture
        JMP End_Of_Display

;//// Change Colours ////////////////////////////////////////////////

HSync_Palette           ; Pick the next colour from that frame's palette
        LDA _Palette_A,Y
        STA COLUPF
        INY             ; Pick the next *next* colour for the next iteration
        TYA
        AND #$03        ; And clamp it's range to the size with fake modulo
        TAY
        JMP Draw_Picture2

VSync_Palette           ; Pick the next frame's ROM palette
        LDA #0          ; But first, reset frame counter
        STA _Frame_Counter

        LDX _Palette_Pointer         ; Get the next palette pointer
        LDA Palette,X   ; Dereference pointer
        STA _Palette_A         ; Store value in RAM palette
        INX             ; Increment pointer
        TXA             ; Copy pointer from X to A
        AND #$03        ; Clamp pointer to range
        TAX             ; Copy pointer from A to X
        LDA Palette,X   ; Second iteration
        STA _Palette_B
        INX
        TXA
        AND #$03
        TAX
        LDA Palette,X   ; Third iteration
        STA _Palette_C
        INX
        TXA
        AND #$03
        TAX
        LDA Palette,X   ; Fourth iteration
        STA _Palette_D
        INX
        INX             ; Increment pointer to next *next* frame's ROM palette
        TXA
        AND #$03        ; Clamp it
        STA _Palette_Pointer         ; Store it in RAM

        JMP Start_Frame2

;////////////// End Of Display ////////////////////////////////////////      	

End_Of_Display
        LDA #%01000010 		; Disable TIA Output
                STA VBLANK           

    ; 30 scanlines of overscan... 

                LDX #30
Overscan        STA WSYNC 
                DEX 
                BNE Overscan 

        JMP Start_Frame 	; Build Next Frame

;//////////// Fetch Notes /////////////////////////////////////

FetchNote
; TODO: Reset note counter and pointer when 0xFFFFFFFFFF is returned.
;       Assume end of song at that point, so it can loop back to beginning.

        LDY #0                  ; Load low byte of pointer

        LDA (_Music_Pointer),Y  ; Load first byte
        TAX                     ; Copy first byte to X
        AND #%00001111          ; Mask out AUDV0
        STA AUDC0               ; So we can get AUDC0
        TXA                     ; Get first byte back from X
        LSR                     ; Shift out AUDC0
        LSR
        LSR
        LSR
        STA AUDV0               ; So we can get AUDV0

        INY                     ; Load second byte
        LDA (_Music_Pointer),Y
        STA AUDF0               ; So we can get AUDF0

        INY                     ; And now do the same with channel 1 {
        LDA (_Music_Pointer),Y
        TAX
        AND #%00001111
        STA AUDC1
        TXA
        LSR
        LSR
        LSR
        LSR
        STA AUDV1

        INY
        LDA (_Music_Pointer),Y
        STA AUDF1               ; }

        INY                     ; And finally...
        LDA (_Music_Pointer),Y  ; Load the fifth byte
        STA _Note_Counter       ; So we can get this note's duration

        CLC                     ; Lastly, have _Music_Pointer point to next note
        LDA _Music_Pointer+0
        ADC #5                  ; Each note is 5 bytes long
        STA _Music_Pointer+0
        LDA _Music_Pointer+1
        ADC #0                  ; Carry the LO into the HI if it overflows...
        STA _Music_Pointer+1    ; ...to support songs longer than FF bytes.

        JMP Start_Frame3

;//////////// Resources ////////////////////////////

    include "lokilogo.asm"
    include "nbflag.asm"
    include "music.asm"

;////////////// Set Vectors ///////////////////////////////////////////

            ORG $FFFA 

; Interrupt Vectors 

            .word Reset           ; NMI 
            .word Reset           ; RESET 
            .word Reset           ; IRQ 

          END 
