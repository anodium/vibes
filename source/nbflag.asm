; Colour palette for the Non-Binary pride flag
; Picked from the TIA NTSC palette

Palette

Yellow
    .byte #$1e

White
    .byte #$0e

Purple
    .byte #$56

Black
    .byte #$04
