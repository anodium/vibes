bin: source/skylar2600.asm source/lokilogo.asm source/nbflag.asm
	mkdir -p build
	dasm source/skylar2600.asm -f3 -Isource -Iinclude -obuild/forskylar.bin -Lbuild/forskylar.lst -sbuild/forskylar.sym

run: bin
	stella build/forskylar.bin

debug: bin
	stella -debug build/forskylar.bin

clean:
	rm -rf build
