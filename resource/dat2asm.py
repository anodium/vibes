#!/usr/bin/env python


def convert(stream):
    lines = stream.split('\n')  # Split stream into lines
    _tmp = list()
    for line in lines:
        _tmp.append(line.lstrip())  # Remove indentation
    lines = _tmp
    del(_tmp)

    packets = []
    _offset = 0
    _tmp = []
    for line in lines:  # Group lines into packets
        _tmp += line.split(',')
        _offset += 1
        if _offset == 3:
            packets.append(_tmp)
            _tmp = []
            _offset = 0

    byte_stream = bytearray()
    for packet in packets:  # Pack packets into raw bytes
        for k, v in enumerate(packet):
            packet[k] = int(v)
        packet = bytes(packet)
        byte_stream.append((packet[0] << 4) + packet[1])
        byte_stream.append(packet[2])
        byte_stream.append((packet[3] << 4) + packet[4])
        byte_stream.append(packet[5])
        byte_stream.append(packet[6])

    print('Music')  # Now, let's spit out those raw bytes as a `dasm` macro
    for b in byte_stream:
        print('    .byte #$' + hex(b).strip('0x').rjust(2, '0'))


if __name__ == '__main__':
    from sys import argv, stdin
    try:  # We have a file, let's open() it
        with open(argv[1], 'r') as f:
            _dat = f.read(-1)
            convert(_dat)
    except IndexError:  # Nevermind! We do NOT have a file, let's use stdin
        _dat = stdin.read(-1)
        convert(_dat)
